#!/usr/bin/env python3

import serial
import time
import csv
from queue import Queue, Empty
from threading import Thread, active_count
from websocket import WebSocket, create_connection
import RPi.GPIO as GPIO
import gw_config

SER_PAYLOAD_MSG_LEN = 24 # 22(payload) + 1(start byte) + 1(end byte) = 24
SER_PAYLOAD_LAST_BYTE = SER_PAYLOAD_MSG_LEN - 2
PAYLOAD_DATA_LEN = 16

# start byte    1   ---
# command       1   [0]
# source addr   1   [1]
# dest addr     1   [2]
# tag           1   [3]
# retry         1   [4]
# rssi          1   [5]
# data          16  [6:21]
# end byte      1   ---

# total 24 bytes

SER_START_BYTES = {
    'GW': 0xbb,
    'RX_DB': 0x55,
    'TX_DB': 0x33
}
SER_END_CHAR = 0xaa

baud = 115200
rx_timeout = 0.1
tx_timeout = 0.1
global ser


def get_time():
    return time.asctime(time.localtime(time.time()))


# Splits a message (bytes) into fields
def parse_message(message: bytes):
    command = message[0]
    source_address = message[1]
    destination_address = message[2]
    tag = message[3]
    retry = message[4]
    rssi = message[5]
    data = message[6:]

    return (
        command,
        source_address,
        destination_address,
        tag,
        retry,
        rssi,
        data
    )

# Attempt to connect to server, retry every second if fail
def websocket_connect() -> WebSocket:
    start_time = 0
    first_retry_fail = True
    while(True):
        try:
            ws = create_connection(gw_config.websocket_ip)
        except:
            check_ser()
            curr_time = time.monotonic()
            if curr_time - start_time >= 5:
                start_time = curr_time
                if first_retry_fail:
                    write_debug_message(gw_config.gw_log_file_path, ["Cannot connect to server"])
                    first_retry_fail = False
        else:
            write_debug_message(gw_config.gw_log_file_path, ["Connected to server"])
            break
    return ws

# Listen to websocket and put messages in a qeue, run this in a separate thread
def websocket_recv(ws: WebSocket, tx: Queue):
    while True:
        try:
            message = ws.recv()
        except:
            break
        else:
            tx.put(message)

# Print and add line to one of the debug csv files with timestamp
def write_debug_message(log_file: str, msg: list):
    msg.insert(0, get_time())
    print(*msg, sep=', ')
    with open(log_file, 'a+') as log_file:
        csv_w = csv.writer(log_file, delimiter=',')
        csv_w.writerow(msg)

# Convert RSSI byte to decibels, uses formula from SPIRIT1 datasheet
def convert_rssi_db(rssi_byte: int) -> int:
    return (rssi_byte / 2 - 130)

# Checks the microcontroller serial port, prints, logs, and returns any received packets
def check_ser() -> bytes:
    global ser
    rx_start_byte: int = int.from_bytes(ser.read(1), 'big') # read the first byte (start byte)

    return_packet = None
    if rx_start_byte in SER_START_BYTES.values(): # make sure the first byte read was a valid start byte
        rx_packet = ser.read(SER_PAYLOAD_MSG_LEN - 1) # read the rest of the packet
        if rx_packet[SER_PAYLOAD_LAST_BYTE] != SER_END_CHAR: # ensure the packet is the right length
            write_debug_message(gw_config.gw_log_file_path, ['INVALID PACKET LENGTH', rx_packet])
        else:
            rx_packet = rx_packet[0:SER_PAYLOAD_LAST_BYTE]  # remove end char
            rx_cmd, rx_src_addr, rx_dest_addr, rx_tag, rx_retry, rssi, rx_data = parse_message(rx_packet)

            log_file_path = ""
            log_label = ""
            if rx_start_byte == SER_START_BYTES['GW']:
                log_file_path = gw_config.gw_log_file_path
                log_label = "FROM DEVICE"
                return_packet = rx_packet
            elif rx_start_byte == SER_START_BYTES['RX_DB']:
                log_file_path = gw_config.radio_debug_log_file_path
                log_label = "RADIO RX"
            elif rx_start_byte == SER_START_BYTES['TX_DB']:
                log_file_path = gw_config.radio_debug_log_file_path
                log_label = "RADIO TX"
            write_debug_message(log_file_path,
                                [log_label,
                                "cmd=" + hex(rx_cmd),
                                "src-addr=" + hex(rx_src_addr),
                                "dest-addr=" + hex(rx_dest_addr),
                                "tag=" + hex(rx_tag),
                                "retry=" + hex(rx_retry),
                                "rssi=" + str(convert_rssi_db(rssi)) + "dB",
                                "data=0x" + str(rx_data.hex())])

        return return_packet

def run():
    write_debug_message(gw_config.gw_log_file_path, ["GW START"])

    # reset the microcontroller - there is some sort of strange power-on behavior when powered by the pi,
    # and it needs to be reset. Useful because when its heartbeat led starts flashing you know the pi
    # is done booting
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(gw_config.reset_pin, GPIO.OUT, initial=GPIO.LOW)
    time.sleep(0.1)
    GPIO.setup(gw_config.reset_pin, GPIO.IN)
    time.sleep(0.1)

    global ser
    try:
        ser = serial.Serial(gw_config.serial_port, baud, parity=serial.PARITY_NONE, \
                stopbits=serial.STOPBITS_ONE, bytesize=serial.EIGHTBITS, timeout=rx_timeout, \
                write_timeout=tx_timeout)
    except:
        write_debug_message(gw_config.gw_log_file_path, ["failed to open serial\n"])
        exit()

    ws = websocket_connect()
    websocket_recv_queue = Queue() # create websocket receive queue
    websocket_recv_thread = Thread(target=websocket_recv, args=(ws, websocket_recv_queue))
    websocket_recv_thread.start() # run websocket_recv_queue() in separate thread

    while True:
        # relay packets received over UART to server via the websocket
        # if sending to websocket fails, then reconnect
        rx_packet = check_ser()
        if rx_packet is not None:
            try:
                ws.send_binary(rx_packet)
            except:
                ws = websocket_connect()

        if active_count() < 2: # recv thread is dead
            ws = websocket_connect() #restart receive thread
            websocket_recv_thread = Thread(target=websocket_recv, args=(ws, websocket_recv_queue))
            websocket_recv_thread.start()

        # get messages from recv queue and relay to microcontroller over UART
        try:
            server_message = websocket_recv_queue.get_nowait()
            if len(server_message) > 0:
                command, source_address, destination_address, tag, \
                        retry, rssi, data = parse_message(server_message)

                write_debug_message(gw_config.gw_log_file_path,
                                    ["FROM SERVER", "cmd=" + hex(command),
                                     "src-addr=" + hex(source_address),
                                     "dest-addr=" + hex(destination_address),
                                     "tag=" + hex(tag), "retry=" + hex(retry), "data=0x" + data.hex()])

                ser.write(int.to_bytes(SER_START_BYTES['GW'], 1, 'big') + server_message + int.to_bytes(SER_END_CHAR, 1, 'big'))
        except Empty:
            pass

        time.sleep(0.01)


run()
